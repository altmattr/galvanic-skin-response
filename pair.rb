class Pair
    attr_accessor :fst, :snd

    def initialize(h, t)
        @fst, @snd = h, t
    end
    def max
        @snd
    end
    def min
        @fst
    end

    def to_s
        "(#{fst},#{snd})"
    end

    def to_string
        "(#{(fst) ? @fst.time : "nil"},#{@snd.time})"
    end

    def self.array_of_to_s(arr)
        str = "["
        arr.each {|t| str = str + t.to_string + ","}
        return (str + "]")
    end

  end 