class Assumptions
	attr :threshold

	def initialize(threshold)
		@threshold = threshold
	end

	def self.Preferred()
		Assumptions.new(0.05)
	end
end