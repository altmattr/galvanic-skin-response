require "./main"
require "yaml"

dataLocation = "C:/Users/mq30513723/Documents/Experiment Data/Processed/"
validateAgainst ="output/late sep 2019/consolidated_output.yml"
toBeValidated = "output/output_0_20.yml"

correct = YAML.load_file(validateAgainst)
maybe = YAML.load_file(toBeValidated)

correct.each do |filename, trialdata|
    trialdata.each do |column, value|
        if(maybe[filename] != nil)
            otherval = maybe[filename][column]
            if (otherval != nil && otherval != value)
                puts "difference on #{filename} #{column}: #{otherval} should be #{value}"
            end
        end
    end
end


