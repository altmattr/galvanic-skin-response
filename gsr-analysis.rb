require "./master"
require "yaml"

main = Main.new
puts "How many files should be ignored?"
ignored = gets
puts "How many files should be processed?"
limit = gets
puts Dir.pwd
res = main.processDirectory(Dir.pwd, limit: limit, ignore: ignored)
File.write("output.yml", res.to_yaml)
Main.writeCSV(res, "output.csv")
puts "press Enter to close"
gets