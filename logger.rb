require "logger"

module Logging
    # This is the magical bit that gets mixed into your classes
    def logger
      Logging.logger
    end

    def setLogLoc(name)
      Logging.setLogLoc(name)
    end

    def self.setLogLoc(name)
      @logger = Logger.new(File.new("logs/"+name+".log", "w"))
    end

  
    # Global, memoized, lazy initialized instance of a logger
    def self.logger
      @logger ||= Logger.new(File.new("logs/gsr.log", "w"))
    end
  end