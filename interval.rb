class Interval
    def initialize(shortName, startString, endString, immediateCutoff: false)
        @shortName = shortName
        @startString = startString
        @endString = endString
        @cutoffStrings = ["CS+ on", "CS- on", "RET end", "EXT end", "PREACQ end", "ACQ end", "US on", "ITI off"]
        @immediateCutoff = immediateCutoff
    end
    attr_accessor :shortName
    attr_accessor :startString
    attr_accessor :endString
    attr_accessor :cutoffStrings
    attr_accessor :immediateCutoff

    def self.FULL()   [Interval.new("CSp"   , CommentPlus.new("CS+ on",1), "CS+ off"                    , immediateCutoff: false),
                       Interval.new("CSm"   , CommentPlus.new("CS- on",1), "CS- off"                    , immediateCutoff: false)] end
    def self.FIR()    [Interval.new("CSp_i1", CommentPlus.new("CS+ on",1), "CS+4sec"                    , immediateCutoff: false),
                       Interval.new("CSm_i1", CommentPlus.new("CS- on",1), "CS-4sec"                    , immediateCutoff: false)] end
    def self.C_SIR()  [Interval.new("CSp_i2", "CS+4sec"                  , "CS+ off"                    , immediateCutoff: false),
                       Interval.new("CSm_i2", "CS-4sec"                  , "CS- off"                    , immediateCutoff: false)] end
    def self.C_TIR()  [Interval.new("CSp_i3", "CS+ off"                  , CommentPlus.new("CS+ off", 4), immediateCutoff: false),
                       Interval.new("CSm_i3", "CS- off"                  , CommentPlus.new("CS- off", 4), immediateCutoff: false)] end
    def self.A_SIR()  [Interval.new("CSp_i2", CommentPlus.new("CS+ on",4), CommentPlus.new("CS+ on", 7) , immediateCutoff: false),
                       Interval.new("CSm_i2", CommentPlus.new("CS- on",4), CommentPlus.new("CS- on", 7) , immediateCutoff: false)] end
    def self.A_TIR()  [Interval.new("CSp_i3", CommentPlus.new("CS+ on",7), CommentPlus.new("CS+ on", 11), immediateCutoff: false),
                       Interval.new("CSm_i3", CommentPlus.new("CS- on",7), CommentPlus.new("CS- on", 11), immediateCutoff: false)] end
    def self.US()     [Interval.new("US"    , "US on"                    , CommentPlus.new("US off", 4) , immediateCutoff: false)] end
    def self.UNLABLED_FULL() [Interval.new("CS", CommentPlus.new("CS on",1), "CS off"                   , immediateCutoff: false)] end

    def commentsOf()
        comments = []
        case (startString)
            when String     ; comments << startString
            when CommentPlus; comments << startString.comment
        end
        case (endString)
            when String     ; comments << endString
            when CommentPlus; comments << endString.comment
        end
        comments
    end
end