class Response
    def initialize(minTime, maxTime, scr, minVal, maxVal)
        @minTime = minTime
        @maxTime = maxTime
        @scr     = scr
        @minVal  = minVal
        @maxVal  = maxVal
    end

    attr_accessor :minTime
    attr_accessor :maxTime
    attr_accessor :scr
    attr_accessor :minVal
    attr_accessor :maxVal

    def to_s()
        return "#{@minTime} -> #{@maxTime} : #{@minVal} -> #{@maxVal} = #{@scr}"
    end
end