require "./logger"


BlockLocalTime = Struct.new(:block, :offset) do
    
    include Logging

    def <=>(other)
        if (self < other)
            return -1
        elsif (self == other)
            return 0
        else
            return 1
        end
    end

    def <(other)
        return block < other.block || (block == other.block && offset < other.offset)
    end

    def >(other)
        return block > other.block || (block == other.block && offset > other.offset)
    end
    def ==(other)
        if (other == nil)
            return block == nil
        end
        return (block == other.block && offset.round(1) == other.offset.round(1)) # TODO: there seems to be a tiny bit of inconsistency coming from using the starting values of blocks with offsets from blocks
    end

    def >=(other); return (self > other || self == other); end
    def <=(other); return (self < other || self == other); end
    def -(num)
         if (num < offset) 
            return BlockLocalTime.new(block, offset - num)
         else
            logger.error("trying to subtract past a block boundary")
            return self
         end 
    end
    
    def +(num); return BlockLocalTime.new(block, offset + num); end # TODO: need to check it does not go over block
   
    def to_s
        return "#{block}@#{offset}"
    end

    def durationPast(other)
        if (block != other.block)
            logger.error("trying to diff between blocks")
            return 0
        else
            return offset - other.offset
        end
    end
end