require "./response"
require "./logger"
require "./block_local_time"

class Analysis
    include Logging

    attr_accessor :doc
    attr_accessor :phase
    attr_accessor :interval

    def initialize(doc, phase, interval)
        @doc = doc
        @phase = phase
        @interval = interval

        @results = Hash.new
    end

    def run()
        foundRejection = false # I will use this whenever I find a reason to reject the whole trial.

        startsOfPhases = @doc.timesOfCommentAfter(@phase.startString, @doc.sof)
        endsOfPhases   = @doc.timesOfCommentAfter(@phase.endString, @doc.sof)

        # pull the right value where there should be just one (i.e. phases)
        if (startsOfPhases.size == 0)
            logger.error "missing phase start for phase #{phase.shortName}"
            return @results
        elsif (startsOfPhases.size > 1)
            startOfPhase = startsOfPhases[-1]
            logger.error "too many phase starts, using last one, got #{startOfPhase}"
        else
            startOfPhase = startsOfPhases[0]
        end

        if (endsOfPhases.size == 0)
            endOfPhase = @doc.eof
            logger.error "missing end of phase, using eof #{@doc.eof}"
        elsif (endsOfPhases.size > 1)
            endOfPhase = endsOfPhases[-1]
            logger.error "too many phase ends, using last one, got #{endOfPhase}"
        else
            endOfPhase = endsOfPhases[0]
        end

        logger.info("using phase start of #{startOfPhase}")
        logger.info("using phase end of #{endOfPhase}")

        repStarts    = @doc.timesOfCommentAfter(@interval.startString, startOfPhase)
        repEnds      = @doc.timesOfCommentAfter(@interval.endString, startOfPhase)
        logger.info "the #{repStarts.size} rep starts are #{repStarts}"
        logger.info "the #{repEnds.size} rep ends are #{repEnds}"

        # TODO: need better logic in here to deal with extra start/ends

        for i in 1..@phase.reps
            logger.info("\n<-- phase #{@phase.shortName}|#{@interval.shortName}, rep #{i} begun -->")

            if (i > [repStarts.size, repEnds.size].min)
                logger.error("we ran out of intervals at #{i}")
                break
            end

            startOfRep = repStarts[i-1]
            endOfRep   = repEnds[i-1]
            logger.info("startOfRep #{i} is #{startOfRep}")
            logger.info("endOfRep #{i} is #{endOfRep}") 

            cutoff = (@interval.immediateCutoff) ? endOfRep : ((@interval.cutoffStrings.map do |s|
                @doc.timesOfCommentAfter(s, endOfRep) 
            end).flatten.min);                                                                               
            if (cutoff == nil)
                logger.error("no cutoff found, using end of rep")
                cutoff = endOfRep
            end
            logger.info("cutoff #{i} is #{cutoff}")

            if (startOfRep > endOfPhase)
                logger.info("interval starts after phase is over - record nothing")
                foundRejection = true
            end
            if (endOfRep == @doc.eof)
                logger.info("end of interval and end of file are the same - record nothing")
                foundRejection = true
            end

            resp = @doc.getResponse(startingFrom: startOfRep, endingAt: endOfRep, cutoff: cutoff) unless foundRejection
            logger.info("got response #{resp}")
            
            if (!foundRejection) # leave blank if the rejection was found here - different to getting null from getResponse, which indicated rising trail
            if (resp)
                # record data from this analysis
                @results["#{@phase.shortName}_#{@interval.shortName}_#{i}_SCR"] =  resp.scr
                @results["#{@phase.shortName}_#{@interval.shortName}_#{i}_min"] =  resp.minVal
                @results["#{@phase.shortName}_#{@interval.shortName}_#{i}_max"] =  resp.maxVal
            else
                @results["#{@phase.shortName}_#{@interval.shortName}_#{i}_SCR"] = nil
                # TODO: if min is meaningful, maybe we need it for min/max as well
            end
            end
        end
        return @results
    end
end