
  * LabChart crashes when trying to close and ignore changes
  * LabChart is hanging on to file descriptors after doc.Close() which makes them unwritable for later instances
  * LabChart is shutting down during processing every now and then.  Seems unable to get to 500 in any case.

Show Stoppers

  * Algorithm cannot work if we ever try to select a period that overlaps a block marker.


WW Comments:

  * A phase will be ignored if it has an end, but not a start.
  * Derivative is very noisy, causing errors in the identification of insignificant dips / rises. The start of an interval can be incorrectly identified as flat / sloped.
  * The algorithm sometimes ignores larger, valid humps.
