require "win32ole"
require "tmpdir"
require_relative "document"
require_relative "analysis"
require_relative "interval"
require_relative "phase"
require_relative "logger"

# TODO: All this code should go in the experiment class, it makes much more sense there!
class Main
    include Logging
    @count = 0
    @limit = 0

    def initialize()
    end

    def runExperiment(exp, limit:600, ignore: 0)
        # do the analysis for this whole directory and get back a hash of file name to (hash of column to value)
        res = processDirectory(exp.input_location, exp.input_matcher, exp.structure, exp.commentsOf(), exp.columns, limit: limit, ignore: ignore)
        # put the result nested hash through any adjustment defined in the experiment definition
        setLogLoc("adjustments")
        if exp.adjustment
            res = exp.adjustment.call(res)
        end
        # write out the yaml (raw) and csv version to the experiment's output folder
        setLogLoc("writing files")
        File.write(exp.output_location + "output_#{ignore}_#{ignore+limit}.yml", res.to_yaml)
        Main.writeCSV(res, exp.name_extractor, exp.output_location + "output_#{ignore}_#{ignore+limit}.csv")
    end

    def processDirectory(path, regex, structure, comments, columns, limit:600, ignore:0)
        @limit = limit
        res = Hash.new
        files = Dir.glob("*", base: path).grep(regex)
        soFar = 0
        puts "Beginning analysis of up to #{limit} files (found #{files.length} ):"
        logger.info("# Beginning analysis of #{limit} files:")
        
        @count = 1;
        files.each  do |item|
            if (soFar >= ignore && soFar < @limit+ignore)
                fullPath = File.path(File.join(path, item))
                setLogLoc(item)
                res[item] = processFile(fullPath, structure, comments, columns)
                @count = @count + 1
            end
            soFar = soFar + 1
        end
        return res
    end

    def processFile(path, amap, comments, columns)
        i = @count
        length = @limit # TODO: these lines can be factored away

        # kill labchart and restart it
        # TODO: not working great, but leaving here for future reference
        # system("taskkill", "/im", "LabChart8.exe", "/f")
        # system("C:/Program Files (x86)/ADInstruments/LabChart8/LabChart8.exe", '')

        puts "processing file #{i}/#{length}: #{File.basename(path)}"
        logger.info("# processing file #{i}/#{length}: #{path}")
        app = WIN32OLE.connect("ADIChart.Application")

        doc = app.open(path)
        doc = Document.new(doc, comments, columns)
        doc.clearDataPad()
        
        
        combinations = Array.new
        amap.each do |phase, intervals|
            intervals.each do |interval|
                combinations << Analysis.new(doc, phase, interval)
            end
        end

        result = combinations.map{|a| 
            puts "...analysing #{a.phase.shortName}:#{a.interval.shortName}"
            logger.info("...analysing #{a.phase.shortName}:#{a.interval.shortName}")
            a.run()
        }.reduce({}, :merge)
        
        # hack to get around the run-time error on close with true parameter.
        # labchart is hanging onto file descriptors which means we need a freash file over and over.
        tempFile = Dir.tmpdir + "/#{(0...8).map { (65 + rand(26)).chr }.join}.adicht"
        doc.saveAs(tempFile) 
        doc.close()
        
        result
    end

    def Main.writeCSV(hhash, name_extractor, filename)
        # get the union of all keys in the inner hashes
        allKeys = Array.new
        hhash.each do |key, value|
            allKeys = allKeys | value.keys
        end
        
        

        File.open(filename, "w") do |file|
            # write header
            file.write("file")
            allKeys.each do |key|
                file.write(",")
                file.write(key)
            end
            file.write("\n")

            # write data
            hhash.each do |key, value|
                # key will be file name from which we can extract primary key
                num = name_extractor.match(key)
                if(num)
                    numstr = num[1]
                else 
                    numstr = "key error - #{key}"
                end
                file.write(numstr)
                allKeys.each do |key|
                    file.write(",")
                    if (value.key?(key))
                        if (value[key])
                            file.write("%.3f" % value[key])  # TODO: what is the required precision
                        else
                            file.write("%.3f" % -99)
                        end
                    else
                        file.write("")
                    end
                end
                file.write("\n")
            end
        end
    end
end