class Phase
    def initialize(shortName, start, endString, reps)
        @shortName = shortName
        @reps = reps
        @startString = start
        @endString = endString
    end
    attr_accessor :shortName
    attr_accessor :reps
    attr_accessor :startString
    attr_accessor :endString

    def self.RET() Phase.new("RET", "RET start", "RET end", 4) end
    def self.PREACQ() Phase.new("PREACQ", "PREACQ start", "PREACQ end", 3) end
    def self.ACQ() Phase.new("ACQ", "ACQ start", "ACQ end", 8) end
    def self.ACQ_US() Phase.new("ACQ", "ACQ start", "ACQ end", 6) end
    def self.EXT() Phase.new("EXT", "EXT start", "EXT end", 8) end
    def self.REC() Phase.new("REC", "EXT start", "EXT end", 8) end
    def self.DOUBLE_PREACQ() Phase.new("PREACQ", "PREACQ start", "PREACQ end", 6) end
    def self.DOUBLE_ACQ() Phase.new("ACQ", "ACQ start", "ACQ end", 16) end
    def self.DOUBLE_EXT() Phase.new("EXT", "EXT start", "EXT end", 16) end

    def commentsOf()
        [startString, endString]
    end
        
    
end