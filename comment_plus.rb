class CommentPlus
    attr_accessor :comment, :offset

    def initialize(c, o)
        @comment, @offset = c, o
    end

    def to_s
        "#{comment} plus #{offset} secs"
    end

  end 