require "win32ole"
require "./response"
require "./logger"
require './pair'
require './table'
require './comment_plus'
require './block_local_time'
require "ostruct"

class Document
    include Logging

    attr_reader :sof
    attr_reader :eof
    attr_reader :comments

    ValueAtTime = Struct.new(:time, :value)
    
    def currBlockNumber() return @doc.GetDataPadCurrentValue(10) end
    def currOffset() return @doc.GetDataPadCurrentValue(4) end
    def currBlockLocalTime() return BlockLocalTime.new(currBlockNumber(), currOffset()); end
    def currCommentText() return @doc.GetDataPadCurrentValue(12) end
    def currValue(sym) return @doc.GetDataPadCurrentValue(@columns[sym]) end # TODO: make much wider use of this

    ##
    # Sets up data pad, finds the start and end of file, populates blocks and creates the extra things
    # we need to query:
    #  * block info in datapad
    #  * derivative channel
    # 
    # Also build a map of blocks in case we need to use it later.
    #
    # Assumes there are some datapad columns already configured as per this experiment.

    def initialize(doc, interestingComments, colMap)
        @doc      = doc
        @blocks   = Hash.new
        @comments = Hash.new
        @columns  = colMap

        logger.info("assuming column map of #{@columns}")

        # add block number, duration, and comment text to data pad so we can resolve more complex times
        logger.info("adding a block number to the data pad")
        @doc.DataPadColumnSetup(10, "Block Number", 3, 1, "")
        @doc.DataPadColumnSetup(11, "Block Duration", 3, 1, "")
        @doc.DataPadColumnSetup(12, "Full Comment Text", 3, 1, "")

        # where is the start of this file?
        @doc.Find(0,0,1,1,1,false, 1, "Start of file", "")
        @sof = BlockLocalTime.new(currBlockNumber,currOffset)

        # how long is this file?
        @doc.Find(0,0,1,1,1,false, 1, "End of file", "")
        @eof = currBlockLocalTime
        alpha_n = @doc.GetDataPadCurrentValue(10)
        logger.info("found eof of #{@eof}, last block number is #{alpha_n}, blocks are #{@blocks}")

        # populate blocks
        @doc.Find(0,0,1,1,1,false, 1, "Start of file", "")
        @alpha_zero = @doc.GetDataPadCurrentValue(10)
        @blocks[@alpha_zero] = currBlockLocalTime().offset
        logger.info("found sof of #{@sof} first block number is #{@alpha_zero}, blocks are #{@blocks}")
        alpha_at = @alpha_zero + 1
        while (alpha_at <= alpha_n)
            @doc.Find(0,0,1,1,1,false, 1, "Start of next block", "")
            @blocks[alpha_at] = currBlockLocalTime().offset
            alpha_at = alpha_at + 1
        end
        logger.info("Found blocks #{Table.new(@blocks)}")

        # populate comments
        # Getcomment times in doc (start time, end time, comment) - return a list of times
        # start and end time are block time
        interestingComments.each do | c |
            @comments[c] = getCommentTimes(@sof, @eof, c)
        end
        logger.info("Found comments #{Table.new(@comments).to_s}")

        # add a derivative channel (in third slot (i.e. channel 2 - indexed from zero in some places, 1 in others))
        logger.info("adding derivative channel")
        @doc.SetChannelDataRangeInUnits(2, 0.000004, -0.000005, "S/s", "") # TODO: these look like hard coded values!  They came from the macro
        @doc.SetChannelCalculation("Derivative", 2, "DerivativeSettings 2 0 10 2 1 51 1 1")
        @doc.SetChannelTitle(2, "rate of change of gsr")
        @doc.SetDisplayedChannels (3)

        logger.info("going to start")
        goToTime(@sof)
    end

    ##
    # Clears the data pad and resets the selection.

    def clearDataPad()
        @doc.SetDataPadCellSelection(1, 1, 2, -1, -1)
        @doc.clear ("Data Pad")
    end

    ##
    # Receives a time input and outputs the time value and the respective µS value at that time.

    def goToTime(time)
        # TODO: what the heck! blocks report one higher than they need to be in here
        @doc.SelectChannel(-1, true)
        @doc.SetSelectionTime(time.block-1,time.offset - @blocks[time.block],time.block-1,time.offset-@blocks[time.block])
        
        return ValueAtTime.new(currBlockLocalTime, @doc.GetDataPadCurrentValue(1))
    end

    ##
    # Receives two time inputs, and selects the corresponding range on the first channel.

    def selectRange(timeA, timeB)
        logger.info("selecting range #{timeA} to #{timeB}")
        @doc.SelectChannel(-1, true)
        @doc.SetSelectionTime(timeA.block-1, timeA.offset - @blocks[timeA.block], timeB.block-1, timeB.offset - @blocks[timeB.block])
    end

    ##
    # Searches right from startingFrom for a comment named by the input, searchString.
    # If nothing is found, will return the current time, otherwise will return the time of the comment.

 def goToComment(searchClause, startingFrom:)
        logger.debug("searching for comment #{searchClause} starting from #{startingFrom}")
        # will return the time of this comment too.
        timeis = goToTime(startingFrom)
        logger.debug("gototime got back #{timeis} when looking for #{startingFrom}")
        case (searchClause)
                when String
                    logger.info("comment is simple string")
                    @doc.Find(0, 0, 1, 1, 1, false, 1, "Search for comment", "JustThisChannel=0;WhatToLookFor="+searchClause+";")
                when CommentPlus
                    logger.info("comment is string and offset #{searchClause}")
                    @doc.Find(0, 0, 1, 1, 1, false, 1, "Search for comment", "JustThisChannel=0;WhatToLookFor="+searchClause.comment+";")
                    comTime = currBlockLocalTime
                    logger.info("comTime is #{comTime} - of class #{comTime.class}")
                    logger.info("comTime plus is #{comTime + searchClause.offset}")
                    goToTime(comTime + searchClause.offset)
                else
                    raise "strange type for search clause #{searchClause} - #{searchClause.class}"
        end
        return currBlockLocalTime
    end

    ##
    # Searches right from startingFrom for a comment named by the input, searchString.
    # If nothing is found, will return the end of file, otherwise will return the time of the comment.

    def goToCommentOrEOF(searchString, startingFrom:)
        logger.info("searching for comment #{searchString} starting from #{startingFrom} - defaulting to EOF")
        newTime = goToComment(searchString, startingFrom: startingFrom)
        logger.info("new time is #{newTime}")
        if newTime == startingFrom
            logger.info("equality succeeded")
            goToTime(@eof)
        end
        return currBlockLocalTime
    end

    ##
    # Will find the next peak of the data, looking right starting from the input (startingFrom). 
    # Will return the time and value of the peak.

    #NoiseThreshold possibly change to 0.00005 to increase accuracy of detection
    def nextPeak(startingFrom)
        goToTime(startingFrom)
        @doc.Find(0, 0, 1, 1, 0, false, 1, "Local maxima", "NoiseThreshold=0.0001;")
        return ValueAtTime.new(currBlockLocalTime(), @doc.GetDataPadCurrentValue(1))
    end

    ##
    # Will find the next trough of the data, looking right starting from the input (startingFrom). 
    # Will return the time and value of the trough.

    #NoiseThreshold possibly change to 0.00005 to increase accuracy of detection
    def nextTrough(startingFrom)
        goToTime(startingFrom)
        @doc.Find(0, 0, 1, 1, 0, false, 1, "Local minima", "NoiseThreshold=0.0001;")
        return ValueAtTime.new(currBlockLocalTime(), @doc.GetDataPadCurrentValue(2))
    end

    ##
    # Will find the last trough of the data, looking left starting from the input (startingFrom). 
    # Will return the time and value of the trough.

    def lastTrough(startingFrom)
        goToTime(startingFrom)
        @doc.Find(0, 0, 1, 1, 0, false, 0, "Local maxima", "NoiseThreshold=0.0001;")
        return ValueAtTime.new(currBlockLocalTime(), @doc.GetDataPadCurrentValue(1))
    end

    ##
    # Finds the first point going forward that has a derivative 0.05 greater than or less than the original derivative value.
    # Search starts from the inputted startingFrom value, and returns the time and value of the derivative at the new point.

    def forwardWhileFlat(startingFrom)
        logger.info("looking forward for end of flatness")
        goToTime(startingFrom)
        @doc.Find(2, 0, 1, 1, 0, false, 1, "Data Above", "Limit=0.05;")
        above = ValueAtTime.new(currBlockLocalTime(), @doc.GetDataPadCurrentValue(1))
        goToTime(startingFrom)
        logger.info("from above it is #{above}")
        @doc.Find(2, 0, 1, 1, 0, false, 1, "Data Below", "Limit=-0.05;")
        below = ValueAtTime.new(currBlockLocalTime(), @doc.GetDataPadCurrentValue(1))
        logger.info("from below it is #{below}")
        if (above.time < below.time)
            logger.info("so we went with above")
            return above
        else
            logger.info("so we went with below")
            return below
        end
    end

    ##
    # Finds the first point going backwards that has a derivative 0.05 greater than or less than the original derivative value.
    # Search starts from the inputted startingFrom value, and returns the time and value of the derivative at the new point.

    def backWhileFlat(startingFrom)
        logger.info("looking forward for end of flatness")
        goToTime(startingFrom)
        @doc.Find(2, 0, 1, 1, 0, false, 0, "Data Above", "Limit=0.05;")
        above = ValueAtTime.new(currBlockLocalTime(), @doc.GetDataPadCurrentValue(1))
        logger.info("from above it is #{above}")
        goToTime(startingFrom)
        @doc.Find(2, 0, 1, 1, 0, false, 0, "Data Below", "Limit=-0.05;")
        below = ValueAtTime.new(currBlockLocalTime(), @doc.GetDataPadCurrentValue(1))
        logger.info("from below it is #{below}")
        if (above.time > below.time)
            logger.info("so we went with above")
            return above
        else
            logger.info("so we went with below")
            return below
        end
    end

    def method_missing(method, *args)
        @doc.send(method, *args)
    end
    
    ##
    # Returns an array of responses, that occur after startingFrom and before endingAt. 

    #TODO: cutoff value not used.
    def searchForResponses(startingFrom, endingAt, cutoff)
        logger.info("searching for responses from #{startingFrom} to #{endingAt}")
        if (startingFrom >= endingAt)
            return []
        end

        trough = nextTrough(startingFrom)
        peak = nextPeak(startingFrom)
        logger.info("peak at #{peak}")
        logger.info("trough at #{trough}")

        if (peak.time == startingFrom) #if the next peak does not move us forward at all.
            return []
        end

        if (peak.time < trough.time)
            start = goToTime(startingFrom)
            if (peak.value - start.value > 0.05) #we are genuinely going up
                return searchForResponses(peak.time, endingAt, cutoff).unshift(Pair.new(nil, peak))
            else
                return searchForResponses(peak.time, endingAt, cutoff)
            end
        else
            return searchForResponses(peak.time, endingAt,cutoff).unshift(Pair.new(trough, peak))
        end
    end

    ##
    # From an array of potential responses, finds the largest response and places it at arr[0]
    # If responses are below 0.05, drops the response

    def consolidateSubThreshold(arr)
        if (arr.size() >= 2)
            one = arr[0]
            two = arr[1]
            if (one.min != nil && one.max.value - two.min.value < 0.05 && one.max.value < two.max.value)
                if (one.min.value > two.min.value) # second one drops lower
                    return consolidateSubThreshold(arr[1,arr.length-1]) # just drop the other, it is not important.
                else
                    return consolidateSubThreshold(arr[2, arr.length-2].unshift(Pair.new(one.min, two.max))) # actually do the consolidation
                end
            else
                return consolidateSubThreshold(arr[1, arr.length-1]).unshift(arr[0]) #keep original and consolidate on tail
            end
        else
            return arr
        end
    end

    ##
    # Applies rules to the responses in the range from startingFrom to endingAt, in order to return a valid response. 
    # The return value will be nil is the only response starts by rising.
    # The return will be zero if there are no recorded responses.
    # Otherwise, the function will return the value of the response in the appropriate region.
    # The cutoff is the last allowed point of consideration

    def getResponse(startingFrom:, endingAt:, cutoff:)
        rs = searchForResponses(startingFrom, endingAt, cutoff)
        logger.info("initial pass shows up #{Pair.array_of_to_s(rs)}")

        rs = rs.select {|tup| tup.min == nil || tup.min.time < cutoff }
        logger.info("after removing any that starts after the cutoff #{Pair.array_of_to_s(rs)}")

        # check any rising to see if they are actually ok
        # it is OK if "there is a period of flatness greater than 1 second and ending after the start+minima"
        if(rs.size() > 0 && rs[0].min == nil)
            # only do this is we are in a flat period at the start
            logger.info("selecting from #{startingFrom} to #{startingFrom+0.001}")
            selectRange(startingFrom , startingFrom + 0.01) # TODO: not happy with this (and next line) as a way to get flatness at this point
            logger.info("data in this range is #{@doc.getSelectedData(0,3)}")
            currentFlatness =  @doc.getSelectedData(0,3)[0]
            logger.info("the flatness at the starting point is #{currentFlatness}")
            if (currentFlatness != nil && currentFlatness.abs < 0.05) # if we can't measure flatness here we will just assume it is rising
                logger.info("first one is rising but flat at the start so we are checking if it is really flat")
                flatEnd = forwardWhileFlat(startingFrom)
                flatStart = backWhileFlat(startingFrom)
                logger.info("flat starts at #{flatStart.time} and ends at #{flatEnd.time}")
                if (flatEnd.time.durationPast(flatStart.time) > 1 && flatEnd.time > startingFrom)
                    logger.info("so this rising response can count")
                    rs[0].fst = goToTime(startingFrom)
                end
            end
        end

        logger.info("after converting any valid rising #{Pair.array_of_to_s(rs)}")

        # if there is a nil in the min slot of any that are not the first, there was a discontinuity in this trial and it should be rejected
        if (rs.size() > 1 && (rs.drop(1).select {|e| e.min == nil}).size > 0)
            logger.error("there was a discontinuity so this trial was rejected")
            return nil
        end
    
        rs = consolidateSubThreshold(rs)
        logger.info("after consolidating #{Pair.array_of_to_s(rs)}")

        rs.each do |tup| 
            if (tup.max.time > cutoff) 
                nvt = goToTime(cutoff)
                tup.snd.time = cutoff
                tup.snd.value = nvt.value 
            end
        end
        logger.info("after capping all responses at the cutoff #{Pair.array_of_to_s(rs)}")

        rs = rs.select {|tup|  (tup.fst == nil) || (tup.max.value - tup.min.value > 0.05)}
        rs.each do |tup|
            if (tup.fst != nil)
                logger.info("#{tup}")
            end
        end
        logger.info("after removing small  #{Pair.array_of_to_s(rs)} ")

        rs = rs.select {|tup| tup.min == nil || tup.min.time < endingAt} # only keep those that begin before the end of the interval
        logger.info("after removing those that start after the interval ends  #{Pair.array_of_to_s(rs)} ")

        # there was only one response and it was rising - return nil
        if ((rs.size() == 1) && (rs[0].min == nil))
            return nil;
        end

        rs = rs.select{ |tup| (tup.fst !=nil)} # there was more than one response but the first one was rising, remove rising responses
        logger.info("after removing residual rising #{Pair.array_of_to_s(rs)} ")
        
        if (rs.size() == 0)
            return Response.new(startingFrom, endingAt, 0, nil, nil)
        end
        selectRange(rs[0].min.time, rs[0].max.time)
        return Response.new(rs[0].min.time, rs[0].max.time, currValue(:diff), currValue(:min), currValue(:max))
    end

    def getCommentTimes(startTime, endTime, comment)
        times = []
        goToTime(startTime)
        @doc.Find(0, 0, 1, 1, 1, false, 1, "Search for comment", "JustThisChannel=0;WhatToLookFor="+comment+";")
        t = currBlockLocalTime
        prev = BlockLocalTime.new(0, -1)
        while t < endTime && !(t == prev)
            if (currCommentText() == comment)
                times << t
            end
            prev = t
            @doc.FindNext()
            t = currBlockLocalTime
        end

        return times
    end

    def timesOfCommentAfter(marker, startingFrom)
        possibles = []
        case (marker)
        when String
            possibles = @comments[marker]
        when CommentPlus
            possibles = @comments[marker.comment].map {|blt| blt + marker.offset}
        else
            logger.error "broken marker #{marker}"
        end
        if (possibles == nil)
            logger.error "unable to find any times for marker #{marker}"
            possibles = []
        end
        possibles.select {|blt| blt >= startingFrom}
    end
end