require_relative "main"
require_relative "experiment"
require "yaml"

limit = 250
ignore = 0

main = Main.new
main.runExperiment(Experiment.MQRF_B , limit:limit, ignore:ignore)
