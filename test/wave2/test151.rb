require "./test/lctest"
require "./experiment"

#WW = Winter Workshop squad.

class TestWaveTwoOneFiveOne < LabChartTestCase
    def test_one_five_one_RET_full
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.RET() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.07, res["RET_CSp_1_SCR"]) #TODO: WW expected 1.2, algo didn't classify start as flat.
        assert_equalish(0.08, res["RET_CSp_2_SCR"]) #TODO: WW expected 0.72, incorrect hump measured.
        assert_equalish(0.64, res["RET_CSp_3_SCR"])
        assert_equalish(0.5,  res["RET_CSp_4_SCR"]) #TODO: WW expected 0.66, algo considering an insignificant drop.
        assert_equalish(nil,    res["RET_CSm_1_SCR"]) #WW expected 0.06, very bearly significant increase.
        assert_equalish(0.76, res["RET_CSm_2_SCR"])
        assert_equalish(0,    res["RET_CSm_3_SCR"])
        assert_equalish(0,    res["RET_CSm_4_SCR"])
    end

    def test_one_five_one_RET_FIR
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.RET() => Interval.FIR()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(nil,  res["RET_CSp_i1_1_SCR"]) #WW expected 1.2, algo didn't classify start as flat.
        assert_equalish(0.08, res["RET_CSp_i1_2_SCR"])
        assert_equalish(0,    res["RET_CSp_i1_3_SCR"])
        assert_equalish(nil,  res["RET_CSp_i1_4_SCR"]) #TODO: WW expected 0.66, incredibly weird result...
        assert_equalish(nil,    res["RET_CSm_i1_1_SCR"]) #WW expected 0.06, very bearly significant increase.
        assert_equalish(0.76, res["RET_CSm_i1_2_SCR"])
        assert_equalish(0,    res["RET_CSm_i1_3_SCR"])
        assert_equalish(0,    res["RET_CSm_i1_4_SCR"])
    end

    def test_one_five_one_RET_SIR
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.RET() => Interval.C_SIR()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.07,  res["RET_CSp_i2_1_SCR"])
        assert_equalish(nil,  res["RET_CSp_i2_2_SCR"]) #WW expected 0.72, incorrectly considered nonflat. 
        assert_equalish(0.64, res["RET_CSp_i2_3_SCR"])
        assert_equalish(0.5,  res["RET_CSp_i2_4_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_1_SCR"])
        assert_equalish(nil,  res["RET_CSm_i2_2_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_3_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_4_SCR"])
    end

    def test_one_five_one_RET_TIR
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.RET() => Interval.C_TIR()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.06, res["RET_CSp_i3_1_SCR"])
        assert_equalish(0.18, res["RET_CSp_i3_2_SCR"]) #TODO: WW not sure bout this one.
        assert_equalish(nil,  res["RET_CSp_i3_3_SCR"]) # WW expected 0.35, algo not seeing start flat.
        assert_equalish(nil,  res["RET_CSp_i3_4_SCR"])
        assert_equalish(0,    res["RET_CSm_i3_1_SCR"])
        assert_equalish(0.4,  res["RET_CSm_i3_2_SCR"])
        assert_equalish(0.15, res["RET_CSm_i3_3_SCR"]) 
        assert_equalish(0,    res["RET_CSm_i3_4_SCR"])
    end

    def test_one_five_one_PREACQ_full
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.PREACQ() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.08, res["PREACQ_CSp_1_SCR"])
        assert_equalish(0.12, res["PREACQ_CSp_2_SCR"]) #TODO: WW expected 0.18, algo thought dip was significant.
        assert_equalish(nil,  res["PREACQ_CSp_3_SCR"]) #TODO: WW expected 0.12, very strange nil.
        assert_equalish(nil,    res["PREACQ_CSm_1_SCR"]) #WW expected 0.06
        assert_equalish(0.1,  res["PREACQ_CSm_2_SCR"])
        assert_equalish(0.1,  res["PREACQ_CSm_3_SCR"]) #TODO: WW expected 0.3, the algo ignores a second, much larger hump, filled with insignificant dips.  
    end

    def test_one_five_one_ACQ_full
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.ACQ() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.13, res["ACQ_CSp_1_SCR"])
        assert_equalish(0,    res["ACQ_CSp_2_SCR"])
        assert_equalish(0.4,  res["ACQ_CSp_3_SCR"])
        assert_equalish(0.22, res["ACQ_CSp_4_SCR"]) #WW expected 1.19, explained by us on being cutoff.
        assert_equalish(0.31, res["ACQ_CSp_5_SCR"]) #WW expected 0.62, us on acts as cutoff.
        assert_equalish(nil , res["ACQ_CSp_6_SCR"]) #TODO: WW expected 0.25, non flat.
        assert_equalish(0,    res["ACQ_CSm_1_SCR"]) #TODO: WW expected 0.06, very interesting case, where multiple insignificant rises join, not detected by algo.
        assert_equalish(0.12, res["ACQ_CSm_2_SCR"]) 
        assert_equalish(0,    res["ACQ_CSm_3_SCR"]) 
        assert_equalish(0.06, res["ACQ_CSm_4_SCR"]) #WW expected 0.08, algo ignores a larger hump.
        assert_equalish(0,    res["ACQ_CSm_5_SCR"])
    end

    def test_one_five_one_ACQ_US
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.ACQ_US() => Interval.US()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(1.28, res["ACQ_US_1_SCR"]) #TODO: WW expected 1.03, algo stopped checking at a weird spot.
        assert_equalish(0.4,  res["ACQ_US_2_SCR"])
        assert_equalish(nil,  res["ACQ_US_3_SCR"]) #TODO: WW expected 0.93, incorrectly determined rising at start.
        assert_equalish(0.24, res["ACQ_US_4_SCR"])
    end

    def test_one_five_one_EXT_full
        res = @main.processFile(@waveTwoLoc + "T2_RAW151_FC_ShapesB.adicht", 
                                 {Phase.EXT() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
            assert_equalish(nil, res["EXT_CSp_1_SCR"])
            assert_equalish(nil, res["EXT_CSp_2_SCR"])
            assert_equalish(nil, res["EXT_CSp_3_SCR"])
            assert_equalish(nil, res["EXT_CSp_4_SCR"])
            assert_equalish(nil, res["EXT_CSp_5_SCR"])
            assert_equalish(nil, res["EXT_CSp_6_SCR"])
            assert_equalish(nil, res["EXT_CSp_7_SCR"])
            assert_equalish(nil, res["EXT_CSp_8_SCR"])
            assert_equalish(nil, res["EXT_CSm_1_SCR"])
            assert_equalish(nil, res["EXT_CSm_2_SCR"]) 
            assert_equalish(nil, res["EXT_CSm_3_SCR"])
            assert_equalish(nil, res["EXT_CSm_4_SCR"])
            assert_equalish(nil, res["EXT_CSm_5_SCR"])
            assert_equalish(nil, res["EXT_CSm_6_SCR"]) 
            assert_equalish(nil, res["EXT_CSm_7_SCR"])
            assert_equalish(nil, res["EXT_CSm_8_SCR"]) 
    end 
end