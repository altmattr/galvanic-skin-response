require "./test/lctest"
require "./experiment"

class TestWaveTwoOhOhTwo < LabChartTestCase

    def test_oh_oh_two_RET_full
        res = @main.processFile(@waveTwoLoc + "T2_RAW002_FC_BellsD.adicht", 
                                {Phase.RET() => Interval.FULL()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.81, res["RET_CSp_1_SCR"])
        assert_equalish(0.23, res["RET_CSp_2_SCR"]) # TODO: Diff to Carly who has 0.0 - OK
        assert_equalish(0.14, res["RET_CSp_3_SCR"])
        assert_equalish(0.08, res["RET_CSp_4_SCR"])
        assert_equalish(0.90, res["RET_CSm_1_SCR"])
        assert_equalish(1.83, res["RET_CSm_2_SCR"]) # TODO: Diff to Carly who has 1.82
        assert_equalish(0.30, res["RET_CSm_3_SCR"])
        assert_equalish(1.46, res["RET_CSm_4_SCR"]) # TODO: Diff to Carly who has 1.46
    end

    def test_oh_oh_two_PREACQ_FIR
        res = @main.processFile(@waveTwoLoc + "T2_RAW002_FC_BellsD.adicht", 
                                 {Phase.PREACQ() => Interval.FIR()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(nil,  res["PREACQ_CSp_i1_1_SCR"])
        assert_equalish(1.50, res["PREACQ_CSp_i1_2_SCR"]) 
        assert_equalish(0.41, res["PREACQ_CSp_i1_3_SCR"]) # TODO: Diff to Carly who has 0.42
        assert_equalish(1.03, res["PREACQ_CSm_i1_1_SCR"])
        assert_equalish(0,  res["PREACQ_CSm_i1_2_SCR"]) 
        assert_equalish(0.00, res["PREACQ_CSm_i1_3_SCR"])
    end

    def test_oh_oh_two_ACQ_SIR
        res = @main.processFile(@waveTwoLoc + "T2_RAW002_FC_BellsD.adicht", 
                                {Phase.ACQ() => Interval.C_SIR()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.64, res["ACQ_CSp_i2_1_SCR"])
        assert_equalish(0.20, res["ACQ_CSp_i2_2_SCR"]) 
        assert_equalish(0.16, res["ACQ_CSp_i2_3_SCR"])
        assert_equalish(0.00, res["ACQ_CSp_i2_4_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_i2_5_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_i2_6_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_i2_7_SCR"])
        assert_equalish(0.87, res["ACQ_CSp_i2_8_SCR"])
        assert_equalish(nil,  res["ACQ_CSm_i2_1_SCR"])
        assert_equalish(0.25, res["ACQ_CSm_i2_2_SCR"]) # TODO: Diff to Carly who has nil
        assert_equalish(0.80, res["ACQ_CSm_i2_3_SCR"]) # TODO: Diff to Carly who has 0.82
        assert_equalish(0.25, res["ACQ_CSm_i2_4_SCR"]) # TODO: Diff to Carly who has 0.11 (there is a double sub-threshold dip)
        assert_equalish(1.06, res["ACQ_CSm_i2_5_SCR"]) 
        assert_equalish(0.91, res["ACQ_CSm_i2_6_SCR"]) # TODO: Diff to Carly who have 0.00
        assert_equalish(0.81, res["ACQ_CSm_i2_7_SCR"]) 
        assert_equalish(0.85, res["ACQ_CSm_i2_8_SCR"]) # TODO: Diff to Carly who has 0.71
    end
end