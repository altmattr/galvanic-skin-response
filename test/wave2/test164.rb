require "./test/lctest"
require "./experiment"

class TestOneSixFour < LabChartTestCase

    def test_one_six_four_RET_full
        res = @main.processFile(@problemFileLoc + "T2_RAW164_FC_BellsD Data.adicht", 
                                 {Phase.RET() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(3.28, res["RET_CSp_1_SCR"])
        assert_equalish(1.91, res["RET_CSp_2_SCR"])
        assert_equalish(1.1,  res["RET_CSp_3_SCR"])
        assert_equalish(0.51, res["RET_CSp_4_SCR"]) #TODO: WW expected 0.58, small dip is insignificant
        assert_equalish(1.15, res["RET_CSm_1_SCR"])
        assert_equalish(0.07, res["RET_CSm_2_SCR"])
        assert_equalish(1.14, res["RET_CSm_3_SCR"]) #WW expected 1.15
        assert_equalish(0,    res["RET_CSm_4_SCR"])
    end

    def test_one_six_four_RET_FIR
        res = @main.processFile(@problemFileLoc + "T2_RAW164_FC_BellsD Data.adicht", 
                                 {Phase.RET() => Interval.FIR()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(3.28, res["RET_CSp_i1_1_SCR"])
        assert_equalish(1.91, res["RET_CSp_i1_2_SCR"])
        assert_equalish(1.1,  res["RET_CSp_i1_3_SCR"])
        assert_equalish(0.51, res["RET_CSp_i1_4_SCR"]) #TODO: WW expected 0.58, small dip is insignificant
        assert_equalish(1.15, res["RET_CSm_i1_1_SCR"])
        assert_equalish(0,    res["RET_CSm_i1_2_SCR"])
        assert_equalish(1.14, res["RET_CSm_i1_3_SCR"]) #WW expected 1.15
        assert_equalish(0,    res["RET_CSm_i1_4_SCR"])
    end

    def test_one_six_four_RET_SIR
        res = @main.processFile(@problemFileLoc + "/T2_RAW164_FC_BellsD Data.adicht", 
                                 {Phase.RET() => Interval.C_SIR()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(nil,  res["RET_CSp_i2_1_SCR"])
        assert_equalish(nil,  res["RET_CSp_i2_2_SCR"])
        assert_equalish(1.05, res["RET_CSp_i2_3_SCR"])
        assert_equalish(nil,  res["RET_CSp_i2_4_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_1_SCR"])
        assert_equalish(0.07, res["RET_CSm_i2_2_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_3_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_4_SCR"])
    end

    def test_one_six_four_RET_TIR
        res = @main.processFile(@problemFileLoc + "T2_RAW164_FC_BellsD Data.adicht", 
                                 {Phase.RET() => Interval.C_TIR()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(nil,  res["RET_CSp_i3_1_SCR"])
        assert_equalish(0,    res["RET_CSp_i3_2_SCR"])
        assert_equalish(0.45, res["RET_CSp_i3_3_SCR"]) 
        assert_equalish(0.22, res["RET_CSp_i3_4_SCR"])
        assert_equalish(0.15, res["RET_CSm_i3_1_SCR"])
        assert_equalish(0,    res["RET_CSm_i3_2_SCR"])
        assert_equalish(1.04, res["RET_CSm_i3_3_SCR"])
        assert_equalish(0,    res["RET_CSm_i3_4_SCR"])
    end

    def test_one_six_four_PREACQ_full
        res = @main.processFile(@problemFileLoc + "T2_RAW164_FC_BellsD Data.adicht", 
                                 {Phase.PREACQ() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(nil,  res["PREACQ_CSp_1_SCR"])
        assert_equalish(nil,  res["PREACQ_CSp_2_SCR"])
        assert_equalish(nil,  res["PREACQ_CSp_3_SCR"])
        assert_equalish(nil,  res["PREACQ_CSm_1_SCR"])
        assert_equalish(nil,  res["PREACQ_CSm_2_SCR"])
        assert_equalish(nil,  res["PREACQ_CSm_3_SCR"])
    end

    def test_one_six_four_ACQ_full
        res = @main.processFile(@problemFileLoc + "T2_RAW164_FC_BellsD Data.adicht", 
                                 {Phase.ACQ() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(nil,  res["ACQ_CSp_1_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_2_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_3_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_4_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_5_SCR"])
        assert_equalish(nil,  res["ACQ_CSp_6_SCR"])
        assert_equalish(nil,  res["ACQ_CSm_1_SCR"]) 
        assert_equalish(nil,  res["ACQ_CSm_2_SCR"]) 
        assert_equalish(nil,  res["ACQ_CSm_3_SCR"]) 
        assert_equalish(nil,  res["ACQ_CSm_4_SCR"]) 
        assert_equalish(nil,  res["ACQ_CSm_5_SCR"])
    end

    def test_one_six_four_EXT_full
        res = @main.processFile(@problemFileLoc + "T2_RAW164_FC_BellsD Data.adicht", 
                                 {Phase.EXT() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
            assert_equalish(nil, res["EXT_CSp_1_SCR"])
            assert_equalish(nil, res["EXT_CSp_2_SCR"])
            assert_equalish(nil, res["EXT_CSp_3_SCR"])
            assert_equalish(nil, res["EXT_CSp_4_SCR"])
            assert_equalish(nil, res["EXT_CSp_5_SCR"])
            assert_equalish(nil, res["EXT_CSp_6_SCR"])
            assert_equalish(nil, res["EXT_CSp_7_SCR"])
            assert_equalish(nil, res["EXT_CSp_8_SCR"])
            assert_equalish(nil, res["EXT_CSm_1_SCR"])
            assert_equalish(nil, res["EXT_CSm_2_SCR"]) 
            assert_equalish(nil, res["EXT_CSm_3_SCR"])
            assert_equalish(nil, res["EXT_CSm_4_SCR"])
            assert_equalish(nil, res["EXT_CSm_5_SCR"])
            assert_equalish(nil, res["EXT_CSm_6_SCR"]) 
            assert_equalish(nil, res["EXT_CSm_7_SCR"])
            assert_equalish(nil, res["EXT_CSm_8_SCR"]) 
    end    
end