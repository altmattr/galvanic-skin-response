require "./test/lctest"
require "./experiment"

class TestWaveTwoOhEightNine < LabChartTestCase

    def test_oh_eight_nine_basics
        res = @main.processFile(@waveTwoLoc + "/T2_RAW089_FC_ShapesB.adicht",  {Phase.RET() => Interval.FULL(), Phase.PREACQ() => Interval.FULL(), Phase.ACQ() => Interval.FULL()}, Experiment.Wave2.commentsOf(), Experiment.Wave2.columns) #phases: [Phase.RET(), Phase.PREACQ(), Phase.ACQ()], intervals: Interval.FULL())
        assert_equalish(0.26, res["RET_CSp_1_SCR"])
        assert_equalish(0.18, res["RET_CSp_2_SCR"])
        assert_equalish(0.20, res["RET_CSp_3_SCR"])
        assert_equalish(0.20, res["RET_CSp_4_SCR"])
        assert_equalish(0.12, res["RET_CSm_1_SCR"])
        assert_equalish(0.16, res["RET_CSm_2_SCR"])
        assert_equalish(0.00, res["RET_CSm_3_SCR"])
        assert_equalish(0.08, res["RET_CSm_4_SCR"])
        assert_equalish(0.14, res["PREACQ_CSp_1_SCR"])
        assert_equalish(0.18, res["PREACQ_CSp_2_SCR"])
        assert_equalish(0.21, res["PREACQ_CSp_3_SCR"])
        assert_equalish(0.12, res["PREACQ_CSm_1_SCR"])
        assert_equalish(0.10, res["PREACQ_CSm_2_SCR"])
        assert_equalish(0.13, res["PREACQ_CSm_3_SCR"])
        assert_equalish(0.00, res["ACQ_CSp_1_SCR"])
        assert_equalish(0.21, res["ACQ_CSp_2_SCR"])
        assert_equalish(nil , res["ACQ_CSp_3_SCR"])
        assert_equalish(nil , res["ACQ_CSp_4_SCR"])
        assert_equalish(nil , res["ACQ_CSp_5_SCR"])
        assert_equalish(nil , res["ACQ_CSp_6_SCR"])
        assert_equalish(nil , res["ACQ_CSp_7_SCR"])
        assert_equalish(nil , res["ACQ_CSp_8_SCR"])
    end
    
end