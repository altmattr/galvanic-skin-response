require "./test/lctest"
require "./experiment"

class TestWaveTwoOhOhOne < LabChartTestCase

    def test_oh_oh_one_RET_full
        res = @main.processFile(@waveTwoLoc + "T2_RAW001_FC_BellsB.adicht", 
                                 {Phase.RET() => Interval.FULL()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0,    res["RET_CSp_1_SCR"])
        assert_equalish(0.06, res["RET_CSp_2_SCR"])
        assert_equalish(0,    res["RET_CSp_3_SCR"])
        assert_equalish(0,  res["RET_CSp_4_SCR"])
        assert_equalish(0,    res["RET_CSm_1_SCR"])
        assert_equalish(0,    res["RET_CSm_2_SCR"])
        assert_equalish(0,    res["RET_CSm_3_SCR"])
        assert_equalish(0.13, res["RET_CSm_4_SCR"])
    end

    def test_oh_oh_one_RET_FIR
        res = @main.processFile(@waveTwoLoc + "T2_RAW001_FC_BellsB.adicht", 
                                 {Phase.RET() => Interval.FIR()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0,    res["RET_CSp_i1_1_SCR"])
        assert_equalish(0,    res["RET_CSp_i1_2_SCR"])
        assert_equalish(0,    res["RET_CSp_i1_3_SCR"])
        assert_equalish(0,    res["RET_CSp_i1_4_SCR"])
        assert_equalish(0,    res["RET_CSm_i1_1_SCR"])
        assert_equalish(0,    res["RET_CSm_i1_2_SCR"])
        assert_equalish(0,    res["RET_CSm_i1_3_SCR"])
        assert_equalish(0.13, res["RET_CSm_i1_4_SCR"])
    end

    def test_oh_oh_one_RET_SIR
        res = @main.processFile(@waveTwoLoc + "/T2_RAW001_FC_BellsB.adicht", 
                                 {Phase.RET() => Interval.C_SIR()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0,    res["RET_CSp_i2_1_SCR"])
        assert_equalish(0.06, res["RET_CSp_i2_2_SCR"])
        assert_equalish(0,    res["RET_CSp_i2_3_SCR"])
        assert_equalish(0,    res["RET_CSp_i2_4_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_1_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_2_SCR"])
        assert_equalish(0,    res["RET_CSm_i2_3_SCR"])
        assert_equalish(nil,  res["RET_CSm_i2_4_SCR"])  # TODO: diff to Carly
    end

    def test_oh_oh_one_RET_TIR
        res = @main.processFile(@waveTwoLoc + "T2_RAW001_FC_BellsB.adicht", 
             {Phase.RET() => Interval.C_TIR()},
             Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0,    res["RET_CSp_i3_1_SCR"])
        assert_equalish(0.06, res["RET_CSp_i3_2_SCR"]) #OK diff to carly
        assert_equalish(0.00, res["RET_CSp_i3_3_SCR"]) 
        assert_equalish(0,    res["RET_CSp_i3_4_SCR"])
        assert_equalish(0,    res["RET_CSm_i3_1_SCR"])
        assert_equalish(0.00, res["RET_CSm_i3_2_SCR"])
        assert_equalish(0,    res["RET_CSm_i3_3_SCR"])
        assert_equalish(0.47, res["RET_CSm_i3_4_SCR"]) # TODO: diff to carly who has 0
    end

    def test_oh_oh_one_PREACQ_full
        res = @main.processFile(@waveTwoLoc + "/T2_RAW001_FC_BellsB.adicht", 
                                 {Phase.PREACQ() => Interval.FULL()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0,    res["PREACQ_CSp_1_SCR"])
        assert_equalish(2.15, res["PREACQ_CSp_2_SCR"]) #TODO: diff to carly by 0.05
        assert_equalish(0,    res["PREACQ_CSp_3_SCR"])
        assert_equalish(0,    res["PREACQ_CSm_1_SCR"])
        assert_equalish(0.18, res["PREACQ_CSm_2_SCR"]) #TODO: diff to Carly (alg sees small rise starting just before cutoff)
        assert_equalish(0,    res["PREACQ_CSm_3_SCR"])
    end

    def test_oh_oh_one_ACQ_full
        res = @main.processFile(@waveTwoLoc + "/T2_RAW001_FC_BellsB.adicht", 
                                 {Phase.ACQ() => Interval.FULL()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(0.05, res["ACQ_CSp_1_SCR"]) # OK diff to Carly
        assert_equalish(nil,  res["ACQ_CSp_2_SCR"]) # TODO: diff to Carly (carly counted rising at the start)
        assert_equalish(2.08, res["ACQ_CSp_3_SCR"]) # TODO: diff to Carly (offset?)
        assert_equalish(1.05, res["ACQ_CSp_4_SCR"]) # TODO: diff to Carly (offset?)
        assert_equalish(1.04, res["ACQ_CSp_5_SCR"])
        assert_equalish(0.73, res["ACQ_CSp_6_SCR"]) # TODO: diff to Carly (offset?)
        assert_equalish(0.69, res["ACQ_CSp_7_SCR"]) # TODO: diff to Carly (no equiv?)
        assert_equalish(0.66, res["ACQ_CSp_8_SCR"]) # TODO: small amount different
        assert_equalish(0.43, res["ACQ_CSm_1_SCR"]) 
        assert_equalish(1.63, res["ACQ_CSm_2_SCR"]) 
        assert_equalish(0.20, res["ACQ_CSm_3_SCR"]) 
        assert_equalish(0.69, res["ACQ_CSm_4_SCR"]) # TODO: diff to Carly (alg picks up a response starting before ITI)
        assert_equalish(0.61, res["ACQ_CSm_5_SCR"]) # TODO: diff to Carly (alg picks up small rise/fall just before the big rise as part of interval because the dip is sub-threshold)
        assert_equalish(0.34, res["ACQ_CSm_6_SCR"]) 
        assert_equalish(0.78, res["ACQ_CSm_7_SCR"])
        assert_equalish(0.00, res["ACQ_CSm_8_SCR"]) 
    end

    def test_oh_oh_one_ACQ_US
        res = @main.processFile(@waveTwoLoc + "/T2_RAW001_FC_BellsB.adicht", 
                                 {Phase.ACQ_US() => Interval.US()},
                                 Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
        assert_equalish(5.5,  res["ACQ_US_1_SCR"]) 
        assert_equalish(5.55, res["ACQ_US_2_SCR"]) # TODO: diff because the front of this one is not counted as flat!  It just dips below -0.05 just a tiny bit early - might need to tweak the threshold.
        assert_equalish(0.65, res["ACQ_US_3_SCR"]) # TODO: tiny bit off
        assert_equalish(1.91, res["ACQ_US_4_SCR"]) 
        assert_equalish(1.08, res["ACQ_US_5_SCR"]) #TODO: tiny bit off
        assert_equalish(0.84, res["ACQ_US_6_SCR"]) 
    end

    def test_oh_oh_one_EXT_full
        res = @main.processFile(@waveTwoLoc + "T2_RAW001_FC_BellsB.adicht", 
             {Phase.EXT() => Interval.FULL()},
             Experiment.Wave2.commentsOf(), Experiment.Wave2.columns)
            assert_equalish(1.79, res["EXT_CSp_1_SCR"])
            assert_equalish(0.43, res["EXT_CSp_2_SCR"])
            assert_equalish(0.00, res["EXT_CSp_3_SCR"])
            assert_equalish(1.82, res["EXT_CSp_4_SCR"])
            assert_equalish(0.61, res["EXT_CSp_5_SCR"])
            assert_equalish(nil,  res["EXT_CSp_6_SCR"]) #TODO: diff to carly - alg is picking up rising at the start because it counts one second in as the start.  Carly is counting that as a response and getting 0.4
            assert_equalish(0.00, res["EXT_CSp_7_SCR"])
            assert_equalish(0.16, res["EXT_CSp_8_SCR"])
            assert_equalish(0.77, res["EXT_CSm_1_SCR"])
            assert_equalish(0.27, res["EXT_CSm_2_SCR"]) # TODO: diff to carly - Carly has 0 but I see a pretty clear rise at - do we have the right location?
            assert_equalish(0.00, res["EXT_CSm_3_SCR"])
            assert_equalish(0.00, res["EXT_CSm_4_SCR"])
            assert_equalish(0.06, res["EXT_CSm_5_SCR"])
            assert_equalish(0.51, res["EXT_CSm_6_SCR"]) # TODO: diff to carly - Carly is counting the rise that starts before 1 second after the start of the interval and so getting a much larger number (0.97)
            assert_equalish(0.36, res["EXT_CSm_7_SCR"])
            assert_equalish(0.42, res["EXT_CSm_8_SCR"]) 
    end    
end