require_relative "../main"
require "test/unit"

class LabChartTestCase < Test::Unit::TestCase

    def setup
        @main = Main.new
        @dataLocation = "C:/Users/mq30513723/Documents/Experiment Data/Processed/"
        @waveOneLoc = "C:/Users/mq30513723/Documents/Experiment Data/WAVE 1/"
        @waveTwoLoc = "C:/Users/mq30513723/Documents/Experiment Data/WAVE 2/"
        @waveThreeLoc = "C:/Users/mq30513723/Documents/Experiment Data/WAVE 3/"
        @problemFileLoc = "C:/Users/mq30513723/Documents/Experiment Data/Wave 2 Problem files/"
    end 

    def assert_equalish(a, b)
        (a == nil) ? assert_nil(b) :  assert_equal(a.round(2), b.round(2))
    end
end
