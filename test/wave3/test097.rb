require "./test/lctest"
require "./experiment"

class TestWaveThreeOhNineSeven < LabChartTestCase

    def test_oh_nine_seven_RET_full
        res = @main.processFile(@waveThreeLoc + "T3_RAW097_18112004_FC_ShapesA.adicht", 
                                 {Phase.RET() => Interval.FULL()},
                                 Experiment.Wave3.commentsOf(), Experiment.Wave3.columns)
        assert_equalish(0.06,    res["RET_CSp_1_SCR"])
        assert_equalish(0.35,    res["RET_CSm_1_SCR"])
        assert_equalish(0.09,    res["RET_CSm_2_SCR"])
    end

end