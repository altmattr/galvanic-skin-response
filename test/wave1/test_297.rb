require "./test/lctest"
require "./experiment"

class TestWaveOneTwoNineSeven < LabChartTestCase
    # This is a file where we were crashing, so just check it work.

    def test_two_nine_seven_doesnt_crash
        file = "/T1_RAW297_FC_BellsA.adicht"
        res = @main.processFile(@waveOneLoc + file, 
                                {Phase.DOUBLE_PREACQ() => (Interval.UNLABLED_FULL())},
                                Experiment.Wave1.commentsOf(), Experiment.Wave1.columns)
        # raw results
        assert_equalish(nil, res["PREACQ_CS_3_SCR"])
        assert_equalish(nil, res["PREACQ_CS_3_min"])
        assert_equalish(nil, res["PREACQ_CS_3_max"])
    end
    
end