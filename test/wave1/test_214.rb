require "./test/lctest"
require "./experiment"

class TestWaveOneTwoOneFour < LabChartTestCase
    # This is a file where we were crashing, so just check it works.

    def test_two_one_four_basics
        file = "/T1_RAW214_FC_BellsA.adicht"
        res = @main.processFile(@waveOneLoc + file, 
                                {Phase.DOUBLE_PREACQ() => (Interval.UNLABLED_FULL())},
                                Experiment.Wave1.commentsOf(), Experiment.Wave1.columns)
        # raw results
        assert_equalish(0.07, res["PREACQ_CS_1_SCR"])
        assert_equalish(3.25, res["PREACQ_CS_1_min"])
        assert_equalish(3.32, res["PREACQ_CS_1_max"])
        assert_equalish(0.356, res["PREACQ_CS_2_SCR"])  # TODO: this one is failing because of bad cson/off markers.  There is one to many cs on so we are choosing the wrong start spot.
        assert_equalish(1.96, res["PREACQ_CS_2_min"])
        assert_equalish(2.31, res["PREACQ_CS_2_max"])

        # adjusting col names
        withFile = {file => res}
        withFile = Experiment.Wave1.adjustment.call(withFile)
        res = withFile[file]
        puts res
        assert_equalish(nil, res["PREACQ_CSp_1_SCR"])
        assert_equalish(nil, res["PREACQ_CSp_1_min"])
        assert_equalish(nil, res["PREACQ_CSp_1_max"])
        assert_equalish(0.356, res["PREACQ_CSm_1_SCR"])
        assert_equalish(1.96, res["PREACQ_CSm_1_min"])
        assert_equalish(2.31, res["PREACQ_CSm_1_max"])
    end
    
end