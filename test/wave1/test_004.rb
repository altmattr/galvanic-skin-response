require "./test/lctest"
require "./experiment"

class TestWaveOneOhOhFour < LabChartTestCase

    def test_oh_oh_four_basics
        file = "/T1_RAW004_FC_BellsD.adicht"
        res = @main.processFile(@waveOneLoc + file, 
                                {Phase.DOUBLE_PREACQ() => (Interval.UNLABLED_FULL())},
                                Experiment.Wave1.commentsOf(), Experiment.Wave1.columns)
        # raw results
        assert_equalish(0, res["PREACQ_CS_1_SCR"])
        assert_equalish(nil, res["PREACQ_CS_1_min"])
        assert_equalish(nil, res["PREACQ_CS_1_max"])
        assert_equalish(0, res["PREACQ_CS_2_SCR"])
        assert_equalish(nil, res["PREACQ_CS_2_min"])
        assert_equalish(nil, res["PREACQ_CS_2_max"])

        # adjusting col names
        withFile = {file => res}
        withFile = Experiment.Wave1.adjustment.call(withFile)
        res = withFile[file]
        assert_equalish(0, res["PREACQ_CSp_1_SCR"])
        assert_equalish(nil, res["PREACQ_CSp_1_min"])
        assert_equalish(nil, res["PREACQ_CSp_1_max"])
        assert_equalish(0, res["PREACQ_CSm_1_SCR"])
        assert_equalish(nil, res["PREACQ_CSm_1_min"])
        assert_equalish(nil, res["PREACQ_CSm_1_max"])
    end
    
end