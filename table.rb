class Table
    def initialize(map)
        @map = map
    end

    def to_s
        res = "\n----------table----------\n"
        @map.each do |k,v|
            res = res + "\t#{k.to_s}\t\t\t=> #{v.to_s}\n"
        end
        res
    end
end