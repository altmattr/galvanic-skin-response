require "./main"
require "yaml"

path = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/wave 3/"

files = Dir.glob("output*.yml", base: path)

allResults = Hash.new

files.each  do |file|
    puts file
    allResults = allResults.merge(YAML.load_file(File.join(path, file)))
end

File.write(path + "/consolidated_output.yml", allResults.to_yaml)
Main.writeCSV(allResults,/(.*)/, path + "/consolidated_output.csv")