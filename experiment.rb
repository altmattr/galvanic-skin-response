class Experiment
    attr :name
    attr :structure
    attr :input_location
    attr :input_matcher
    attr :output_location
    attr :name_extractor
    attr :adjustment
    attr :columns


    def initialize(input_location, input_matcher, output_location, name, structure, name_extractor, columns, &adjustment)
        @input_location = input_location
        @input_matcher = input_matcher
        @output_location = output_location
        @name = name
        @structure = structure
        @name_extractor = name_extractor
        @adjustment = adjustment
        @columns = columns
    end

    def commentsOf()
        comments = []
        structure.keys.each do |phase|
            comments = comments + phase.commentsOf()
        end
        structure.values.flatten.each do |interval|
            comments = comments + interval.commentsOf()
        end
        comments.uniq
    end

    def self.Wave2
        input_location =  "C:/Users/mq30513723/Documents/Experiment Data/WAVE 2/"
        input_matcher = /.*\.adicht/
        output_location = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/wave 2/"
        name = "wave 2"
        structure = {Phase.RET()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.PREACQ() => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ_US() =>  Interval.US()                                                      ,
                     Phase.EXT()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR())
        }
        name_extractor = /.*(\d{3}).*/
        columns = {:min => 2,
                   :max => 1,
                   :diff => 3,
                   :start => 4,
                   :end => 5
                  }
           
        Experiment.new(input_location, input_matcher, output_location, name, structure, name_extractor, columns)
    end

    def self.Wave3
        input_location =  "C:/Users/mq30513723/Documents/Experiment Data/WAVE 3/"
        input_matcher = /.*\.adicht/
        output_location = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/wave 3/"
        name = "wave 3"
        structure = {Phase.RET()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.PREACQ() => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ_US() =>  Interval.US()                                                      ,
                     Phase.EXT()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR())
        }
        columns = {:min => 2,
                   :max => 1,
                   :diff => 3,
                   :start => 4,
                   :end => 5
                  }
           
        name_extractor = /.*RAW(\d{3})_.*/
        Experiment.new(input_location, input_matcher, output_location, name, structure, name_extractor, columns)
    end

    def self.FC_A
        input_location = "C:/Users/mq30513723/Documents/Experiment Data/FC/"
        input_matcher = /.*ACQ.*EXT.*\.adicht/
        output_location = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/fc/"
        name = "fc"
        structure = {Phase.PREACQ() => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ_US() =>  Interval.US()                                                      ,
                     Phase.EXT()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR())
        }
        columns = {:min => 2,
                   :max => 1,
                   :diff => 3,
                   :start => 4,
                   :end => 5
                  }

        name_extractor = /.*(\d{3}).*/
        Experiment.new(input_location, input_matcher, output_location ,name, structure, name_extractor, columns)
    end
    def self.FC_B
        input_location ="C:/Users/mq30513723/Documents/Experiment Data/FC/"
        input_matcher = /.*REC.*\.adicht/
        output_location = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/fc_b/"
        name = "fc b"
        structure = {Phase.REC() => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                    }
        columns =  {:min => 2,
                    :max => 1,
                    :diff => 3,
                    :start => 4,
                    :end => 5
                   }
        name_extractor = /.*(\d{3}).*/
        Experiment.new(input_location, input_matcher, output_location ,name, structure, name_extractor, columns)
    end

    def self.Wave1
        input_location ="C:/Users/mq30513723/Documents/Experiment Data/Wave 1/"
        input_matcher = /.*\.adicht/
        output_location = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/wave 1/"
        name = "wave 1"
        structure = {Phase.DOUBLE_PREACQ() => (Interval.UNLABLED_FULL()),
                     Phase.DOUBLE_ACQ() => (Interval.UNLABLED_FULL() + Interval.US()),
                     Phase.DOUBLE_EXT() => (Interval.UNLABLED_FULL())
                    }
        columns = {:min => 1,
                   :max => 2,
                   :diff => 3,
                   :start => 4,
                   :end => 5
                  }
        name_extractor = /.*(RAW\d{3}).*/
        Experiment.new(input_location, input_matcher, output_location ,name, structure, name_extractor, columns) do |results|
            results.each do |filename, hash|
                case filename 
                when /.*(a|A|b|B)\.adicht/
                    hash.transform_keys! do |key|
                        case key
                        when /PREACQ_CS(\D*)1_(...)/; "PREACQ_CSp#{$1}1_#{$2}"
                        when /PREACQ_CS(\D*)2_(...)/; "PREACQ_CSm#{$1}1_#{$2}"
                        when /PREACQ_CS(\D*)3_(...)/; "PREACQ_CSm#{$1}2_#{$2}"
                        when /PREACQ_CS(\D*)4_(...)/; "PREACQ_CSp#{$1}2_#{$2}"
                        when /PREACQ_CS(\D*)5_(...)/; "PREACQ_CSm#{$1}3_#{$2}"
                        when /PREACQ_CS(\D*)6_(...)/; "PREACQ_CSp#{$1}3_#{$2}"
                        when /ACQ_CS(\D*)1_(...)/; "ACQ_CSp#{$1}1_#{$2}"
                        when /ACQ_CS(\D*)2_(...)/; "ACQ_CSm#{$1}1_#{$2}"
                        when /ACQ_CS(\D*)3_(...)/; "ACQ_CSp#{$1}2_#{$2}"
                        when /ACQ_CS(\D*)4_(...)/; "ACQ_CSm#{$1}2_#{$2}"
                        when /ACQ_CS(\D*)5_(...)/; "ACQ_CSp#{$1}3_#{$2}"
                        when /ACQ_CS(\D*)6_(...)/; "ACQ_CSm#{$1}3_#{$2}"
                        when /ACQ_CS(\D*)7_(...)/; "ACQ_CSp#{$1}4_#{$2}"
                        when /ACQ_CS(\D*)8_(...)/; "ACQ_CSm#{$1}4_#{$2}"
                        when /ACQ_CS(\D*)9_(...)/; "ACQ_CSp#{$1}5_#{$2}"
                        when /ACQ_CS(\D*)10_(...)/; "ACQ_CSm#{$1}5_#{$2}"
                        when /ACQ_CS(\D*)11_(...)/; "ACQ_CSp#{$1}6_#{$2}"
                        when /ACQ_CS(\D*)12_(...)/; "ACQ_CSm#{$1}6_#{$2}"
                        when /ACQ_CS(\D*)13_(...)/; "ACQ_CSm#{$1}7_#{$2}"
                        when /ACQ_CS(\D*)14_(...)/; "ACQ_CSp#{$1}7_#{$2}"
                        when /ACQ_CS(\D*)15_(...)/; "ACQ_CSm#{$1}8_#{$2}"
                        when /ACQ_CS(\D*)16_(...)/; "ACQ_CSp#{$1}8_#{$2}"
                        when /EXT_CS(\D*)1_(...)/; "EXT_CSp#{$1}1_#{$2}"
                        when /EXT_CS(\D*)2_(...)/; "EXT_CSm#{$1}1_#{$2}"
                        when /EXT_CS(\D*)3_(...)/; "EXT_CSm#{$1}2_#{$2}"
                        when /EXT_CS(\D*)4_(...)/; "EXT_CSp#{$1}2_#{$2}"
                        when /EXT_CS(\D*)5_(...)/; "EXT_CSm#{$1}3_#{$2}"
                        when /EXT_CS(\D*)6_(...)/; "EXT_CSp#{$1}3_#{$2}"
                        when /EXT_CS(\D*)7_(...)/; "EXT_CSm#{$1}4_#{$2}"
                        when /EXT_CS(\D*)8_(...)/; "EXT_CSp#{$1}4_#{$2}"
                        when /EXT_CS(\D*)9_(...)/; "EXT_CSm#{$1}5_#{$2}"
                        when /EXT_CS(\D*)10_(...)/; "EXT_CSp#{$1}5_#{$2}"
                        when /EXT_CS(\D*)11_(...)/; "EXT_CSp#{$1}6_#{$2}"
                        when /EXT_CS(\D*)12_(...)/; "EXT_CSm#{$1}6_#{$2}"
                        when /EXT_CS(\D*)13_(...)/; "EXT_CSp#{$1}7_#{$2}"
                        when /EXT_CS(\D*)14_(...)/; "EXT_CSm#{$1}7_#{$2}"
                        when /EXT_CS(\D*)15_(...)/; "EXT_CSp#{$1}8_#{$2}"
                        when /EXT_CS(\D*)16_(...)/; "EXT_CSm#{$1}8_#{$2}"
                        else; key
                        end
                    end
                when /.*(c|C|d|D)\.adicht/
                    hash.transform_keys! do |key|
                        case key
                        when /PREACQ_CS(\D*)1_(...)/; "PREACQ_CSm#{$1}1_#{$2}"
                        when /PREACQ_CS(\D*)2_(...)/; "PREACQ_CSp#{$1}1_#{$2}"
                        when /PREACQ_CS(\D*)3_(...)/; "PREACQ_CSp#{$1}2_#{$2}"
                        when /PREACQ_CS(\D*)4_(...)/; "PREACQ_CSm#{$1}2_#{$2}"
                        when /PREACQ_CS(\D*)5_(...)/; "PREACQ_CSp#{$1}3_#{$2}"
                        when /PREACQ_CS(\D*)6_(...)/; "PREACQ_CSm#{$1}3_#{$2}"
                        when /ACQ_CS(\D*)1_(...)/; "ACQ_CSp#{$1}1_#{$2}"
                        when /ACQ_CS(\D*)2_(...)/; "ACQ_CSm#{$1}1_#{$2}"
                        when /ACQ_CS(\D*)3_(...)/; "ACQ_CSm#{$1}2_#{$2}"
                        when /ACQ_CS(\D*)4_(...)/; "ACQ_CSp#{$1}2_#{$2}"
                        when /ACQ_CS(\D*)5_(...)/; "ACQ_CSm#{$1}3_#{$2}"
                        when /ACQ_CS(\D*)6_(...)/; "ACQ_CSp#{$1}3_#{$2}"
                        when /ACQ_CS(\D*)7_(...)/; "ACQ_CSm#{$1}4_#{$2}"
                        when /ACQ_CS(\D*)8_(...)/; "ACQ_CSp#{$1}4_#{$2}"
                        when /ACQ_CS(\D*)9_(...)/; "ACQ_CSp#{$1}5_#{$2}"
                        when /ACQ_CS(\D*)10_(...)/; "ACQ_CSm#{$1}5_#{$2}"
                        when /ACQ_CS(\D*)11_(...)/; "ACQ_CSp#{$1}6_#{$2}"
                        when /ACQ_CS(\D*)12_(...)/; "ACQ_CSm#{$1}6_#{$2}"
                        when /ACQ_CS(\D*)13_(...)/; "ACQ_CSm#{$1}7_#{$2}"
                        when /ACQ_CS(\D*)14_(...)/; "ACQ_CSp#{$1}7_#{$2}"
                        when /ACQ_CS(\D*)15_(...)/; "ACQ_CSm#{$1}8_#{$2}"
                        when /ACQ_CS(\D*)16_(...)/; "ACQ_CSp#{$1}8_#{$2}"
                        when /EXT_CS(\D*)1_(...)/; "EXT_CSm#{$1}1_#{$2}"
                        when /EXT_CS(\D*)2_(...)/; "EXT_CSp#{$1}1_#{$2}"
                        when /EXT_CS(\D*)3_(...)/; "EXT_CSp#{$1}2_#{$2}"
                        when /EXT_CS(\D*)4_(...)/; "EXT_CSm#{$1}2_#{$2}"
                        when /EXT_CS(\D*)5_(...)/; "EXT_CSp#{$1}3_#{$2}"
                        when /EXT_CS(\D*)6_(...)/; "EXT_CSm#{$1}3_#{$2}"
                        when /EXT_CS(\D*)7_(...)/; "EXT_CSp#{$1}4_#{$2}"
                        when /EXT_CS(\D*)8_(...)/; "EXT_CSm#{$1}4_#{$2}"
                        when /EXT_CS(\D*)9_(...)/; "EXT_CSp#{$1}5_#{$2}"
                        when /EXT_CS(\D*)10_(...)/; "EXT_CSm#{$1}5_#{$2}"
                        when /EXT_CS(\D*)11_(...)/; "EXT_CSm#{$1}6_#{$2}"
                        when /EXT_CS(\D*)12_(...)/; "EXT_CSp#{$1}6_#{$2}"
                        when /EXT_CS(\D*)13_(...)/; "EXT_CSm#{$1}7_#{$2}"
                        when /EXT_CS(\D*)14_(...)/; "EXT_CSp#{$1}7_#{$2}"
                        when /EXT_CS(\D*)15_(...)/; "EXT_CSm#{$1}8_#{$2}"
                        when /EXT_CS(\D*)16_(...)/; "EXT_CSp#{$1}8_#{$2}"
                        else; key
                        end
                    end
                else
                    puts "found a strange file"
                end
            end
            results
        end
    end
    def self.MQRF_A
        input_location = "C:/Users/mq30513723/Documents/Experiment Data/MQRF/"
        input_matcher = /.*ACQ.*\.adicht/
        output_location = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/mqrf_a/"
        name = "mqrf a"
        structure = {Phase.PREACQ() => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                     Phase.ACQ_US() =>  Interval.US()                                                      ,
                     Phase.EXT()    => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR())
        }
        columns = {:min => 2,
                   :max => 1,
                   :diff => 3,
                   :start => 4,
                   :end => 5
                  }

        name_extractor = /.*(\d{3}).*/
        Experiment.new(input_location, input_matcher, output_location ,name, structure, name_extractor, columns)
    end
    def self.MQRF_B
        input_location ="C:/Users/mq30513723/Documents/Experiment Data/MQRF/"
        input_matcher = /.*RE(C|T).*\.adicht/i
        output_location = "C:/Users/mq30513723/Documents/Experiment Data/analysis files/output/mqrf_b/"
        name = "mqrf b"
        structure = {Phase.REC() => (Interval.FULL() + Interval.FIR()+Interval.A_SIR()+Interval.A_TIR()),
                    }
        columns =  {:min => 2,
                    :max => 1,
                    :diff => 3,
                    :start => 4,
                    :end => 5
                   }
        name_extractor = /.*(\d{3}).*/
        Experiment.new(input_location, input_matcher, output_location ,name, structure, name_extractor, columns)
    end
end

